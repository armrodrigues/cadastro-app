from django.contrib import admin
from django.urls import path, include
from core.views import index

urlpatterns = [
    path('admin/', admin.site.urls),

    # Web Server Routes
    path('', index, name='home'),
    path('users/', include('core.urls')),

    # Api Routes
    path('api/v1/', include('api.urls')),
]
