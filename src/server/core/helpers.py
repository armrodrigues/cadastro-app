import csv
from openpyxl import Workbook
from django.http import HttpResponse
from .models import AppUser
from django.core import serializers

def generate_json():
    data = AppUser.objects.all()
    response = HttpResponse(serializers.serialize('json', data), content_type='application/json')
    response['Content-Disposition'] = 'attachment; filename=export.json'
    return response


def generate_csv():
    data = AppUser.objects.all()

    meta = AppUser._meta
    field_names = [field.name for field in meta.fields]

    response = HttpResponse(content_type="text/csv")
    response['Content-Disposition'] = "attachment; filename=users_export.csv"

    writer = csv.writer(response)

    writer.writerow(field_names)

    for obj in data:
        row = writer.writerow([getattr(obj, field) for field in field_names])

    return response


def generate_xlsv():
    data = AppUser.objects.all()
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=users.xlsx'

    workbook = Workbook()
    workbook.iso_dates = True
    
    # Get active worksheet/tab
    worksheet = workbook.active
    worksheet.title = 'Users'

    # Define the titles for columns
    columns = [
        'Id',
        'Email',
        'Password',
        'Birthday',
        'Create_At',
        'Update_At',
    ]
    
    row_num = 1

    for col_num, column_title in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title

    for user in data:
        row_num += 1
        # Define the data for each cell in the row 
        row = [
            str(user.id),
            user.email,
            user.password,
            user.birthday,
            user.create_at.date(),
            user.update_at.date()
        ]
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = cell_value

    workbook.save(response)
    return response
