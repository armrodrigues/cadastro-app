from django.urls import path
from . import views


urlpatterns = [
    path("form", views.form_user),
    path("create", views.create_user, name="create"),
    path("<uuid:pk>", views.show_user, name="show"),
    path("edit/<uuid:pk>", views.edit_user, name="edit"),
    path("update/<uuid:pk>", views.update_user, name="update"),
    path("delete/<uuid:pk>", views.delete_user, name="delete"),
    path("json", views.export_json),
    path("csv", views.export_csv),
    path("xlsx", views.export_xlsv)
]