from django.forms import ModelForm
from core.models import AppUser


class AppUserForm(ModelForm):
    class Meta:
        model = AppUser
        fields = ['email', 'password', 'birthday']