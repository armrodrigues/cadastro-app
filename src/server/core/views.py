from django.shortcuts import redirect, render
from core.forms import AppUserForm
from core.models import AppUser
import random
import string
from core.helpers import *

def index(request):
    data = {}
    data['data'] = AppUser.objects.all()
    return render(request, "user/index.html", data)


def form_user(request):
    data = {}
    data['form'] = AppUserForm()
    return render(request, "user/form.html", data)


def create_user(request):
    form = AppUserForm(request.POST or None)

    if form.is_valid():
        instance = form.save(commit=False)
        if not instance.password:
            instance.password = "".join(random.choice(string.ascii_letters) for i in range(12))
        instance.save()
        return redirect("home")


def show_user(request, pk):
    data = {}
    data["data"] = AppUser.objects.get(pk=pk)
    return render(request, "user/show.html", data)


def edit_user(request, pk):
    data = {}
    data["data"] = AppUser.objects.get(pk=pk)
    data["form"] = AppUserForm(instance=data["data"])
    return render(request, "user/form.html", data) 


def update_user(request,pk):
    data = {}
    data["data"] = AppUser.objects.get(pk=pk)

    form = AppUserForm(request.POST or None, instance=data["data"])

    if form.is_valid():
        form.save()
        return redirect("home")


def delete_user(request, pk):
    data = AppUser.objects.get(pk=pk)
    data.delete()
    return redirect("home")


def export_json(request):
    return generate_json()


def export_csv(request):
    return generate_csv()


def export_xlsv(request):
    return generate_xlsv()