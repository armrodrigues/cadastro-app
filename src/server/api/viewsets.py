from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from core.models import AppUser
from .serializers import AppUserSerializer


class AppUserViewSet(viewsets.ModelViewSet):
    serializer_class = AppUserSerializer
    queryset = AppUser.objects.all()
