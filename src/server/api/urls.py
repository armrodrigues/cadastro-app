from rest_framework import routers
from .viewsets import AppUserViewSet


router = routers.DefaultRouter()
router = routers.DefaultRouter(trailing_slash=False)
router.register('users', AppUserViewSet, 'users')

urlpatterns = router.urls