from datetime import datetime
from rest_framework import serializers
from core.models import AppUser
import random
import string


class AppUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUser
        fields = "__all__"


    def validate_password(self, value):
        if value == "":
            value = "".join(random.choice(string.ascii_letters) for i in range(12))
        return value

    def validate_birthday(self, value):
        if (datetime.today().year - value.year) > 142:
            raise serializers.ValidationError("Invalid Birthday, you cannot create an account using this date of birth. Try again later")
        else:
            return value
