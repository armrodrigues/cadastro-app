import React, { useState, useEffect } from "react";
import "../styles/NavMenuComponent.css";

function NavMenuComponent() {
    const url = "http://127.0.1:8000/users/";
    return (
        <React.Fragment>
            <div className="NavMenuComponent">
                <div className="dropdown">
                    <button className="dropbtn">exportar</button>

                    <div className="dropdown-content">
                        <a
                            href={url + "json"}
                            target="_blank"
                            rel="noreferrer noopener">
                            JSON
                        </a>
                        <a
                            href={url + "csv"}
                            target="_blank"
                            rel="noreferrer noopener">
                            CSV
                        </a>
                        <a
                            href={url + "xlsx"}
                            target="_blank"
                            rel="noreferrer noopener">
                            XLSX
                        </a>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default NavMenuComponent;
