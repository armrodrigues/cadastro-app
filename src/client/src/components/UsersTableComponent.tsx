import { Link } from "react-router-dom";
import User from "../models/User";
import AppEndpoints from "../services/AppEndpoints";
import "../styles/UsersTableComponent.css";

interface Props {
    users: Array<User>;
}

function displayBirthday(value: string) {
    const newValue = value.split("-");
    return newValue[2] + "/" + newValue[1] + "/" + newValue[0];
}

function UsersTableComponent(props: Props) {
    return (
        <div className="UsersTableComponent">
            <table className="table">
                <thead className="table-header">
                    <tr>
                        <th>Email</th>
                        <th>Aniversário</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {props.users.map((user, idx) => {
                        return (
                            <tr key={idx}>
                                <td>{user.email}</td>
                                <td>{displayBirthday(user.birthday)}</td>
                                <td>
                                    <Link to={AppEndpoints.users + user.id}>
                                        Visualizar
                                    </Link>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default UsersTableComponent;
