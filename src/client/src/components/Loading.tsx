import "../styles/Loading.css";

function Loading() {
    return (
        <div className="Loading">
            <div className="loading-icon"></div>
        </div>
    );
}

export default Loading;
