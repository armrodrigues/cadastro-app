import React, { useState } from "react";

//year-month-date
function BirthdayComponent() {
    const [formData, setFormData] = useState({
        day: "",
        month: "",
        year: "",
    });

    const [dayErrStyle, setDayErrStyle] = useState(false);
    const [yearErrStyle, setYearErrStyle] = useState(false);

    const handleDay = (event: React.ChangeEvent<HTMLInputElement>) => {
        let value = Number(event.currentTarget.value);
        if (value >= 1 && value <= 31) {
            setFormData({ ...formData, day: event.currentTarget.value });
            setDayErrStyle(false);
        } else {
            setDayErrStyle(true);
        }
    };

    const handleMonth = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setFormData({ ...formData, month: event.currentTarget.value });
    };

    const handleYear = (event: React.ChangeEvent<HTMLInputElement>) => {
        let year = Number(event.currentTarget.value);
        if (year >= 1180) {
            setFormData({ ...formData, year: event.currentTarget.value });
            setYearErrStyle(false);
        } else {
            setYearErrStyle(true);
        }
    };

    return (
        <div className="BirthdayComponent">
            <br />
            <span className="info">Data de Nascimento</span>
            <br />
            <input
                type="text"
                name="day"
                placeholder="Dia"
                onChange={handleDay}
                className={dayErrStyle ? "input-invalid" : ""}
                required
            />
            <select
                id="month"
                name="month"
                defaultValue={"00"}
                onChange={handleMonth}
                required>
                <option value="00" disabled>
                    Mês
                </option>
                <option value="01">Janeiro</option>
                <option value="02">Fevereiro</option>
                <option value="03">Março</option>
                <option value="04">Abril</option>
                <option value="05">Maio</option>
                <option value="06">Junho</option>
                <option value="07">Julho</option>
                <option value="08">Agosto</option>
                <option value="09">Setembro</option>
                <option value="10">Outubro</option>
                <option value="11">Novembro</option>
                <option value="12">Dezembro</option>
            </select>
            <input
                type="text"
                name="year"
                placeholder="Ano"
                onChange={handleYear}
                className={yearErrStyle ? "input-invalid" : ""}
                required
            />
        </div>
    );
}

export default BirthdayComponent;
