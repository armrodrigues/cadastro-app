interface User {
    id?: string;
    email: string;
    password: string;
    birthday: string;
    create_at?: string;
    update_at?: string;
}

export default User;
