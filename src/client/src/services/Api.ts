interface HttpResponse<T> extends Response {
    parsedBody?: T[];
}
const serverPath: string = "http://localhost:8000/api/v1/";

class Api {
    static async http<T>(request: RequestInfo): Promise<HttpResponse<T>> {
        const response: HttpResponse<T> = await fetch(request);
        try {
            // may error if there is no body
            response.parsedBody = (await response.json()) as Array<T>;
        } catch (ex) {
            console.log(ex);
        }

        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response;
    }

    static async get<T>(endpoint: string): Promise<HttpResponse<T>> {
        const path = serverPath + endpoint;
        const args: RequestInit = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        };
        return await this.http<T>(new Request(path, args));
    }

    static async post<T>(endpoint: string, body: T): Promise<HttpResponse<T>> {
        const path = serverPath + endpoint;
        const args = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json, text/plain, */*",
            },
            body: JSON.stringify(body),
        };
        return await this.http<T>(new Request(path, args));
    }
}

export default Api;
