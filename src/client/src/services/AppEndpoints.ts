class AppEndpoints {
    static home = "/";
    static users = "/usuarios/";
}

export default AppEndpoints;
