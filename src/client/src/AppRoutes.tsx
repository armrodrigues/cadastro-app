import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Homepage from "./pages/Homepage";
import UsersPage from "./pages/user/UsersPage";
import UserDetail from "./pages/user/UserDetail";
import AppEndpoints from "./services/AppEndpoints";

function AppRoutes() {
    return (
        <React.Fragment>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Homepage />} />
                    <Route path={AppEndpoints.users} element={<UsersPage />} />
                    <Route
                        path={AppEndpoints.users + ":id"}
                        element={<UserDetail />}
                    />
                </Routes>
            </BrowserRouter>
        </React.Fragment>
    );
}

export default AppRoutes;
