import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import BirthdayComponent from "../components/BirthdayComponent";
import User from "../models/User";
import Api from "../services/Api";
import AppEndpoints from "../services/AppEndpoints";

function UserForm() {
    const [formData, setFormData] = useState({
        email: "",
        password: "",
        confirmPassword: "",
        birthday: "",
    });

    const navigate = useNavigate();

    const handlePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFormData({ ...formData, password: event.currentTarget.value });
    };

    async function handleSubmit(event: any) {
        event.preventDefault();

        const data = new FormData(event.currentTarget);

        let body: User = {
            email: String(data.get("email")),
            password: String(data.get("password")),
            birthday: String(
                data.get("year") +
                    "-" +
                    data.get("month") +
                    "-" +
                    data.get("day")
            ),
        };

        const response = await Api.post<User>("users", body);

        if (response.status === 201) {
            navigate(AppEndpoints.users);
        }
    }

    return (
        <React.Fragment>
            <form onSubmit={handleSubmit}>
                <input
                    type="email"
                    name="email"
                    className=""
                    placeholder="E-mail"
                    required
                />
                <input
                    type="password"
                    name="password"
                    className=""
                    placeholder="Senha"
                    onChange={handlePassword}
                />
                {formData.password !== "" ? (
                    <input
                        type="password"
                        name="password-check"
                        className=""
                        placeholder="Confirme a Senha"
                    />
                ) : null}
                <BirthdayComponent />
                <input type="submit" value="Cadastrar" />
            </form>
        </React.Fragment>
    );
}

export default UserForm;
