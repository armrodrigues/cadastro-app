import React from "react";
import { Link } from "react-router-dom";
import UserForm from "../forms/UserForm";
import AppEndpoints from "../services/AppEndpoints";
import "../styles/Homepage.css";
import "../styles/UserForm.css";

class Homepage extends React.Component {
    render() {
        return (
            <div className="Homepage">
                <div className="sidebar-image"></div>
                <div className="UserForm">
                    <div className="topbar">
                        <Link to={AppEndpoints.users}>Usuários</Link>
                    </div>
                    <div className="panel">
                        <div className="welcome">
                            <span className="info">bem vindo!</span>
                            <h1>
                                Preencha o formulário para fazer parte da equipe
                            </h1>
                        </div>
                        <UserForm />
                    </div>
                    <div className="navbar">
                        <div>
                            <span className="info">Social</span>
                        </div>

                        <div className="social-icons">
                            <div className="whatsapp"></div>
                            <div className="twitter"></div>
                            <div className="facebook"></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Homepage;
