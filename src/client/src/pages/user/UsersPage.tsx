import React from "react";
import Loading from "../../components/Loading";
import NavMenuComponent from "../../components/NavMenuComponent";
import UsersTableComponent from "../../components/UsersTableComponent";
import User from "../../models/User";
import Api from "../../services/Api";
import ApiEndpoint from "../../services/ApiEndpoints";
import "../../styles/UsersPage.css";

const initialState = {
    isDataFetched: false,
    data: Array<User>(),
};

type State = typeof initialState;

class UsersPage extends React.Component<{}, State> {
    state = initialState;

    async componentDidMount() {
        this.fetchData();
    }

    async fetchData() {
        const data = await Api.get<User>(ApiEndpoint.users);
        if (data.status === 200) {
            const { parsedBody } = data;

            this.setState({
                isDataFetched: true,
                data: parsedBody as unknown as Array<User>,
            });
        }
    }

    handleMenus() {}

    render() {
        return (
            <React.Fragment>
                <div className="UsersPage">
                    <div className="card-header">
                        <div className="label">
                            <h1>Usuários</h1>
                            <div className="nav-menu-container">
                                <NavMenuComponent />
                            </div>
                        </div>
                    </div>

                    {this.state.isDataFetched ? (
                        <UsersTableComponent users={this.state.data} />
                    ) : (
                        <Loading />
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default UsersPage;
