import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import Loading from "../../components/Loading";
import User from "../../models/User";
import Api from "../../services/Api";
import ApiEndpoints from "../../services/ApiEndpoints";
import AppEndpoints from "../../services/AppEndpoints";
import "../../styles/UserDetail.css";

function UserDetail() {
    const { id } = useParams();
    const [isDataFetched, setIsDataFetched] = useState(false);
    const [user, setUser] = useState<User>();

    useEffect(() => {
        async function fetchData() {
            const responseSubjects = await Api.get<User>(
                ApiEndpoints.users + "/" + id
            );
            if (responseSubjects.status === 200) {
                let values = responseSubjects.parsedBody as unknown as User;
                setUser(values);
                setIsDataFetched(true);
            }
        }
        fetchData();
    }, []);

    return (
        <React.Fragment>
            {isDataFetched ? (
                <div className="UserDetail">
                    <div className="card-header">
                        <div className="label">
                            <h1>Usuários</h1>
                        </div>
                    </div>
                    <div className="back-link">
                        <Link to={AppEndpoints.users}>{"<<"} Voltar</Link>
                    </div>

                    <div className="details">
                        <p>
                            <b>Id:</b> {user?.id}
                        </p>
                        <p>
                            <b>E-mail:</b> {user?.email}
                        </p>
                        <p>
                            <b>Senha:</b> {user?.password}
                        </p>
                        <p>
                            <b>Data de Aniversário:</b> {user?.birthday}
                        </p>
                        <p>
                            <b>Registrado em:</b> {user?.create_at}
                        </p>
                        <p>
                            <b>Última atualização:</b> {user?.update_at}
                        </p>
                    </div>
                </div>
            ) : (
                <Loading />
            )}
        </React.Fragment>
    );
}

export default UserDetail;
