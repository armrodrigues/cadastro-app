**Cadastro App**

## FrontEnd

Para rodar a aplicação em modo desenvolvimento deve seguir os seguintes passos:

1. Ter instalado na máquina a versão do nodejs 14.16+
2. Abra o terminal do sistema operacional e navegue até local onde foi clonado o repositório
3. Na raiz do diretório client, executar o comando npm install, para instalar as dependências.
4. Depois da instalação, executar o comando npm start, para iniciar o servidor.
5. O servidor irá rodar na porta 3000, para acessar na máquina local utilize o endereço http://127.0.0.1:3000

---

## Django

1. Ter instalado na máquina o python 3.10+
2. Abra o terminal do sistema operacional e navegue até local onde foi clonado o repositório
3. (Opicional) Crie um ambiente virtual para trabalhar com o python com o seguinte comando: python venv venv
   <br />3.1. No Windows para ativar o ambiente, utilize o comando: ./venv/Scripts/Activate.ps1 ou ./venv/Scripts/Activate.bat
4. Na raiz do diretório server, execute o comando: pip install -r requirements.txt
5. Para executar o servidor rode o comando: python manage.py runserver
6. O servidor irá rodar na porta 8000, para acessar na máquina local utilize o endereço http://127.0.0.1:8000

Dependendo do sistema os comandos a ser executado devem ser python3 e pip3

---

## Utilização dos serviços

1. A aplicação cliente possui as seguinte rotas:
   <br />1.1. http://127.0.0.1:3000
   <br />1.2. http://127.0.0.1:3000/usuarios

2. A aplicação servidor possui as seguintes rotas:
   <br />2.1. http://127.0.0.1:8000
   <br />2.1. http://127.0.0.1:8000/users/json
   <br />2.1. http://127.0.0.1:8000/users/csv
   <br />2.1. http://127.0.0.1:8000/users/xlsx

3. Para requisições REST:
   <br />3.1 http://127.0.0.1:8000/api/v1/users
